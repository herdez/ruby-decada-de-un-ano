## Ejercicio - Decada de un año

Crea el método `decade` que recibe un año y regresa la década del año para el siglo XX.


```ruby
#Driver code

p decade(1905) == "Hundreds"
p decade(1914) == "Tens"
p decade(1920) == "Twenties"
p decade(1935) == "Thirties"
p decade(1943) == "Forties"
p decade(1952) == "Fifties"
p decade(1960) == "Sixties"
p decade(1975) == "Seventies"
p decade(1982) == "Eighties"
p decade(1999) == "Nineties"
p decade(1925) == "Twenties"
```